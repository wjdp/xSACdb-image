# xsacdb-image

Docker image for CI and deployment of xSACdb. Previously used wjdp/flatcar.

## Build

docker build -t wjdp/xsacdb-image:tag .
docker push wjdp/xsacdb-image
